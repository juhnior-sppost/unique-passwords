# Sentinel Unique Passwords Change Log

This project follows [Semantic Versioning](CONTRIBUTING.md).

## Proposals

We do not give estimated times for completion on `Accepted` Proposals.

- [Accepted](https://github.com/cartalyst/sentinel-unique-passwords/labels/Accepted)
- [Rejected](https://github.com/cartalyst/sentinel-unique-passwords/labels/Rejected)

---

### v2.0.0 - 2015-02-24

- Updated for Laravel 5.

`REVISED`

- Switched to PSR-2.

### v1.0.0 - 2014-10-08

- Enforce unique passwords to prevent users from using the same password more than once.
